import * as firebase from 'firebase';

var config = {
  apiKey: "AIzaSyD5H7TGjoxEReizEscfSaVLVKMf0WSyXoQ",
  authDomain: "psswrds-de8fc.firebaseapp.com",
  databaseURL: "https://psswrds-de8fc.firebaseio.com",
  projectId: "psswrds-de8fc",
  storageBucket: "psswrds-de8fc.appspot.com",
  messagingSenderId: "41533693908"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();

export {
  auth,
};